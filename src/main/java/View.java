import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

public class View {
    private DataBase db = new DataBase();

    public void mainSwitcher(String text) throws SQLException, IOException {
        switch (text){
            case "1":
                db.readManual("ManualOfTypicalProblem");
                break;
            case "2":
                db.readManual("SoftwareManual");
                break;
            case "3":
                System.out.println("Enter how do you want to find a problem: \n'1' - Name \n'2' - Type  \n'3' - NameAndType: ");
                String search = bufferReader();
                searchProblem(search);
                break;
            case "4":
                System.out.println("Enter what would you do with Manuals: \n'1' to Create row in manual \n'2' to Update manual \n'3' to Delete row in manual ");
                String work = bufferReader();
                crudSwitcherInManual(work);
                break;
            default:
                System.out.println("Incorrect word! Enter another word!");
        }
    }

    public static String bufferReader() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

        return bf.readLine();
    }

    private void searchProblem(String text) throws IOException, SQLException {
        switch(text){
            case "1":
                System.out.println("Enter name for search: ");
                db.searchByName(bufferReader());
                break;
            case "2":
                System.out.println("Enter type for search: ");
                db.searchByType(bufferReader());
                break;
            case "3":
                System.out.println("Enter name: ");
                String name = bufferReader();
                System.out.println("Enter type: ");
                String type = bufferReader();
                db.searchByNameAndType(type, name);
                break;
            default:
                System.out.println("Incorrect word! Enter another word!");
        }
    }

    private void updater(String nameToUpdate) throws SQLException, IOException {
        if(nameToUpdate.equals("ManualOfTypicalProblem")){
            System.out.println("Enter next variable: Id, Column(Exist typeProblem, nameProblem, descriptionProblem, solutionProblem, popularity), Value");
        }
        else if(nameToUpdate.equals("SoftwareManual")){
            System.out.println("Enter next variable: Id, Column(Exist type, name, description), Value");
        }
        System.out.println("Enter Id");
        int id = Integer.parseInt(bufferReader());
        System.out.println("Enter Column");
        String column = bufferReader();
        System.out.println("Enter Value");
        String value = bufferReader();
        db.updateManual(nameToUpdate, id, column, value);
    }

    private void crudSwitcherInManual(String text) throws IOException, SQLException {
        switch(text){
            case "1":
                System.out.println("Enter name of Manual: \n'1' - ManualOfTypicalProblem \n'2' - SoftwareManual");
                String nameToCreate = bufferReader();
                switch (nameToCreate) {
                    case "1": {
                        System.out.println("Enter next variable: TypeProblem, NameProblem, DescriptionProblem , Solution");
                        System.out.println("Enter type");
                        String type = bufferReader();
                        System.out.println("Enter name");
                        String nameProblem = bufferReader();
                        System.out.println("Enter description");
                        String desc = bufferReader();
                        System.out.println("Enter solution");
                        String solution = bufferReader();
                        db.createInProblemManual(type, nameProblem, desc, solution);
                        break;
                    }
                    case "2": {
                        System.out.println("Enter next variable: Type, Name, Description");
                        String type = bufferReader();
                        String nameProblem = bufferReader();
                        String desc = bufferReader();
                        db.createInManual(type, nameProblem, desc);
                        break;
                    }
                    default:
                        System.out.println("Incorrect word! Enter another word!");
                        break;
                }
                break;
            case "2":
                System.out.println("Enter name of Manual: \n'1' - ManualOfTypicalProblem \n'2' - SoftwareManual");
                String nameToUpdate = bufferReader();
                switch (nameToUpdate) {
                    case "1":
                        updater("ManualOfTypicalProblem");
                        break;
                    case "2":
                        updater("SoftwareManual");
                        break;
                    default:
                        System.out.println("Incorrect word! Enter another word!");
                        break;
                }
                break;
            case "3":
                System.out.println("Enter name of Manual: \n'1' - ManualOfTypicalProblem  \n'2' - SoftwareManual");
                String nameToDelete = bufferReader();
                switch (nameToDelete) {
                    case "1": {
                        System.out.println("Enter next variable: Id");
                        int id = Integer.parseInt(bufferReader());
                        db.deleteManual("ManualOfTypicalProblem", id);
                        break;
                    }
                    case "2": {
                        System.out.println("Enter next variable: Id");
                        int id = Integer.parseInt(bufferReader());
                        db.deleteManual("SoftwareManual", id);
                        break;
                    }
                    default:
                        System.out.println("Incorrect word! Enter another word!");
                        break;
                }
                break;
            default:
                System.out.println("Incorrect word! Enter another word!");
        }
    }
}
