import java.io.IOException;
import java.sql.SQLException;

public class Main {


    public static void main(String[] args) throws IOException, SQLException {
        View view = new View();
        do {
            System.out.println("Menu " +
                    "\nSelect option: " +
                    "\nEnter '1' to read Manual of Typical Problem" +
                    "\nEnter '2' to read Manual of Software" +
                    "\nEnter '3' to search your problem in Manual of typical problems" +
                    "\nEnter '4' to edit Manual of Typical Problem" +
                    "\nEnter '5' to exit");

            String text = View.bufferReader();
            if(text.equals("5")){
                break;
            }
            view.mainSwitcher(text);
        }
        while (true);
    }
}
