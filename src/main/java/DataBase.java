import java.sql.*;

public class DataBase {
    private static Connection connect = null;

    DataBase() {
        try {
            connect = DriverManager.getConnection("jdbc:sqlite:C:\\glassfish4\\DB\\Catalog",
                    "admin", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createInProblemManual(String typeProblem, String nameProblem, String descriptionProblem , String solution) throws SQLException {
        PreparedStatement stmt = connect.prepareStatement(" INSERT INTO ManualOfTypicalProblem (typeProblem, nameProblem, descriptionProblem, solutionProblem) VALUES (?, ?, ?, ?)");
        stmt.setString(1, typeProblem);
        stmt.setString(2, nameProblem);
        stmt.setString(3, descriptionProblem);
        stmt.setString(4, solution);
        stmt.executeUpdate();
    }

    public void createInManual(String type, String name, String description) throws SQLException {
        PreparedStatement stmt = connect.prepareStatement(" INSERT INTO SoftwareManual VALUES (?, ?, ?)");
        stmt.setString(1, type);
        stmt.setString(2, name);
        stmt.setString(3, description);
        stmt.executeUpdate();
    }

    public void arrayFromProblemBase(ResultSet rs) throws SQLException {
        while(rs.next()){
            System.out.println("Problem № " + rs.getString(1));
            System.out.println("Type: " + rs.getString(2));
            System.out.println("Name: " + rs.getString(3));
            System.out.println("Description: " + rs.getString(4));
            System.out.println("Solution: " + rs.getString(5));
            System.out.println("Popularity: " + rs.getString(6) + " likes \n------------------");
        }

    }

    public void arrayFromBase(ResultSet rs) throws SQLException {
        while(rs.next()){
            System.out.println("Problem № " + rs.getString(1));
            System.out.println("Type: " + rs.getString(2));
            System.out.println("Name: " + rs.getString(3));
            System.out.println("Description: " + rs.getString(4) + "\n------------------");
        }

    }

    public void readManual(String nameManual) throws SQLException {
        if(nameManual.equals("SoftwareManual")) {
            PreparedStatement stmt = connect.prepareStatement(" SELECT * FROM SoftwareManual");
            ResultSet rs = stmt.executeQuery();
            arrayFromBase(rs);
        }
        else if(nameManual.equals("ManualOfTypicalProblem")){
            PreparedStatement stmt = connect.prepareStatement(" SELECT * FROM ManualOfTypicalProblem");
            ResultSet rs = stmt.executeQuery();
            arrayFromProblemBase(rs);
        }
    }

    public void updateManual(String nameManual, int id, String column, String value) throws SQLException {
        PreparedStatement stmt = connect.prepareStatement(" UPDATE " + nameManual + " SET " + column +  " = ? WHERE id = " + id);
        stmt.setString(1, value);
        stmt.executeUpdate();
    }

    public void deleteManual(String nameManual, int id) throws SQLException{
        PreparedStatement stmt = connect.prepareStatement(" DELETE FROM " + nameManual + " WHERE  id = " + id);
        stmt.executeUpdate();
    }

    public void searchByType(String type) throws SQLException{
        PreparedStatement stmt = connect.prepareStatement(" SELECT * FROM ManualOfTypicalProblem WHERE typeProblem = ?");
        stmt.setString(1, type);
        ResultSet rs = stmt.executeQuery();
        arrayFromProblemBase(rs);
    }

    public void searchByName(String name) throws SQLException{
        PreparedStatement stmt = connect.prepareStatement(" SELECT * FROM ManualOfTypicalProblem WHERE nameProblem = ?");
        stmt.setString(1, name);
        ResultSet rs = stmt.executeQuery();
        arrayFromProblemBase(rs);
    }

    public void searchByNameAndType(String type, String name)throws SQLException{
        PreparedStatement stmt = connect.prepareStatement(" SELECT * FROM ManualOfTypicalProblem WHERE  typeProblem = ? AND nameProblem = ?");
        stmt.setString(1, type);
        stmt.setString(2, name);
        ResultSet rs = stmt.executeQuery();
        arrayFromProblemBase(rs);
    }


}